import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const platform = const MethodChannel('com.posconnect.paxpay/transaction');
  String _resultString = 'None';

  Future<void> _initiateTransaction() async {
    setState(() {
      _resultString = "Request initiated...";
    });
    String resultString;
    try {
      // Sends the request to Android Portion.
      // No data is being passed, the call just invokes the method "handleMethodCall()"
      // in MainActivity class of Android Portion
      // it then gets routed to
      final result = await platform.invokeMethod('sale');
      resultString = result?.toString() ?? "Null Response";
    } catch (error) {
      resultString = error.toString();
    }

    // refreshes the UI and updates it with transaction result
    setState(() {
      _resultString = resultString;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Transaction Response:',
              ),
              Text(
                '${_resultString}',
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _initiateTransaction,
        tooltip: 'Start Transaction',
        child: Icon(Icons.payment),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
