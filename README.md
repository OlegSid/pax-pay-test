# paxpay

Test app to test pax payment integration.

## Basic functionality
When started, the app will display standard Fluter/Android activity code with header, body and action button in the lower right corner.
When button is pressed the transaction with hardcoded parameters is started. 
After transaction is complete body will display transaction result in the simple string.
Monitor console to see debug logs. 


## Technical Info
This project is a standard Flutter project created from standard New Flutter Application template. 
Flutter part is providing UI and business logic, and Android part handles Pax PosLink integration.

Android Java code is located in `android` folder and is triggered from `lib/main.dart` when button is pressed.

`sale()` function in `/android/app/src/main/java/com/posconnect/paxpay/MainActivity.java` is where all the transaction logic is.
POSLInk is initialized in `configureFlutterEngine()` function which is called when Flutter engine is attached to Android activity(in other words when the app starts) 

## Getting Started

1. install flutter and configure an IDE
the installation and configuration is very easy and straight forward. 

Flutter Framework installation:
https://flutter.dev/docs/get-started/install

Configuring IDE
https://flutter.dev/docs/get-started/editor?tab=vscode

2. Clone and Open this repo in your IDK






###Original readme content with few useful links. 

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
