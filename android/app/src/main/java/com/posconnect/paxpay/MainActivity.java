package com.posconnect.paxpay;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;

import com.pax.poslink.CommSetting;
import com.pax.poslink.POSLinkAndroid;
import com.pax.poslink.PaymentRequest;
import com.pax.poslink.PaymentResponse;
import com.pax.poslink.PosLink;
import com.pax.poslink.ProcessTransResult;
import com.pax.poslink.poslink.POSLinkCreator;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
  private static final String TAG = "PaxPaymentPlugin";
  private static final String CHANNEL = "com.posconnect.paxpay/transaction";
  
  
  MainThreadResult _mainThreadResult;
  PosLink _posLink;
  CommSetting _commSettings;
  
  
  // Once an Activity is created, and its associated FlutterEngine is executing Dart code, 
  // the Activity should invoke this method. At that point the FlutterEngine is considered 
  // "attached" to the Activity and all ActivityAware plugins are given access to the Activity.
  @Override
  public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine){
    super.configureFlutterEngine(flutterEngine);
    
    // configure Pax POSLink when app is initiated.
    initializePosLink(getApplicationContext());
    
    // set method call listener to interface with Flutter 
    new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL)
        .setMethodCallHandler((call, result) -> handleMethodCall(call, result)
    );
  }

  /// sets communication settings for Pax PosLink
  private CommSetting getCommSettings(){
    Log.d(TAG, "Initializing comm settings");
    CommSetting commSettings = new CommSetting();
    commSettings.setType(CommSetting.AIDL);
    commSettings.setTimeOut("-1");
    return commSettings;
  }

  /// Initializes Pax PosLink
  private void initializePosLink(Context context){
    Log.d(TAG, "Initializing PosLink");
    CommSetting commSettings = getCommSettings();
    _posLink = POSLinkCreator.createPoslink(getApplicationContext());
    _posLink.SetCommSetting(commSettings);
    POSLinkAndroid.init(context, commSettings);
    Log.d(TAG, "POSlink is set to: " + _posLink.GetCommSetting().getType());
  }
  
  
  // This is the "Entry Point" for transaction call
  // this function is triggered when the method call is received from Flutter portion
  // Only 'sale' call is expected for now. 
  // For test purpose, this only triggers the transaction, all transaction parameters are hardcoded in sale() function.
  private void handleMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result ) {
    
    // creating result wrapper to avoid possible issues with EventSync.
    // _mainThreadResult will also handle sending the result back to Flutter
    _mainThreadResult = new MainThreadResult(result);
    
    switch (call.method) {
      case "sale":
        sale(call);
        break;
      case "void":
        _mainThreadResult.notImplemented();
        break;
      case "refund":
        _mainThreadResult.notImplemented();
        break;
      case "refundPayment":
        _mainThreadResult.notImplemented();
        break;
      case "authorize":
        _mainThreadResult.notImplemented();
        break;
      case "capture":
        _mainThreadResult.notImplemented();
        break;
      case "offlineAuthorize":
        _mainThreadResult.notImplemented();
        break;
      case "verify":
        _mainThreadResult.notImplemented();
        break;
      case "displayTransaction":
        _mainThreadResult.notImplemented();
        break;
      default:
        _mainThreadResult.notImplemented();
        break;
    }
  }
  

  void sale(MethodCall call) {
    Log.d(TAG, "Sale Transaction is started");

    PaymentRequest paymentRequest = new PaymentRequest();
    paymentRequest.TenderType = paymentRequest.ParseTenderType("CREDIT");
    paymentRequest.TransType = paymentRequest.ParseTransType("SALE");

    long unixTime = System.currentTimeMillis() / 1000L;
    paymentRequest.ECRRefNum = String.valueOf(unixTime);
    paymentRequest.Amount = "11";
    _posLink.PaymentRequest = paymentRequest;
    Log.d(TAG, "Payment Request to be processed: ID-" + paymentRequest.ECRRefNum + " for " + paymentRequest.Amount);

    final ProcessTransResult[] processTransResult = new ProcessTransResult[1];

    new Thread(new Runnable() {
      public void run() {
        Log.d(TAG, "Starting transaction");
        processTransResult[0] = _posLink.ProcessTrans();
      }
    }).start();

    int i = 0;
    while (processTransResult[0] == null && i < 60) {
      Log.d(TAG, "Waiting...  " + i + " seconds elapsed");
      try {
        Thread.sleep(1000);
        i++;
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    assert processTransResult[0] != null;
    Log.d(TAG, "ProcessTranResult: " + processTransResult[0].toString());
    Log.d(TAG, "ProcessTranResult Response: " + processTransResult[0].Response);
    Log.d(TAG, "ProcessTranResult Msg: " + processTransResult[0].Msg);

    if (processTransResult[0].Code == ProcessTransResult.ProcessTransResultCode.ERROR
        || processTransResult[0].Code == ProcessTransResult.ProcessTransResultCode.TimeOut) {
      _mainThreadResult.error(processTransResult[0].Code.toString(), processTransResult[0].Msg, null);
      return;
    }

    if (_posLink.PaymentResponse != null) {
      PaymentResponse paymentResponse = _posLink.PaymentResponse;
      Log.d(TAG, "Payment Response: " + paymentResponse.toString());
      _mainThreadResult.success("OK");
    } else {
      Log.d(TAG, "Payment Response is NULL");
      _mainThreadResult.error("NULL", "Payment Response is NULL", null);
    }
  }
}
