/*
 * Copyright (c) 2020.  POSconnect
 * All right reserved
 * For more info contact support@posconnect.com
 *
 * Author: Oleg Sidorenkov
 * Email: oleg@carrotcat.ca or oleg.a.sidorenkov@gmail.com
 */

package com.posconnect.paxpay;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import io.flutter.plugin.common.MethodChannel;


/// [MainThreadResult] is a wrapper to create fix for EventSink issues
/// For more info see: https://github.com/flutter/flutter/issues/34993
/// Cause of the crash: https://github.com/flutter/engine/commit/2c9e37c34e79475bbde7c8163eb5e56cdb9662a1
class MainThreadResult implements MethodChannel.Result {
  private static final String TAG = "MainThreadResult";
  private MethodChannel.Result _result;
  private Handler handler;
  private long debugId;

  MainThreadResult(MethodChannel.Result result) {
    this.debugId = System.currentTimeMillis();
    this._result = result;
    handler = new Handler(Looper.getMainLooper());
  }

  @Override
  public void success(final Object result) {
    handler.post(
      new Runnable() {
        @Override
        public void run() {
          Log.d(TAG, "Returning success result to flutter. Response debug ID: "+ debugId);
          _result.success(result);
        }
      });
  }

  @Override
  public void error(
    final String errorCode, final String errorMessage, final Object errorDetails) {
      handler.post(
        new Runnable() {
          @Override
          public void run() {
            Log.d(TAG, "Returning error result to flutter. Response debug ID: "+ debugId);
            _result.error(errorCode, errorMessage, errorDetails);
          }
        });
  }

  @Override
  public void notImplemented() {
    handler.post(
      new Runnable() {
        @Override
        public void run() {
          Log.d(TAG, "Returning not implemented result to flutter. Response debug ID: "+ debugId);
          _result.notImplemented();
        }
      });
  }
}